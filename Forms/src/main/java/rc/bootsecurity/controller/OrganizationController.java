package rc.bootsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import rc.bootsecurity.model.Organization;
import rc.bootsecurity.service.OrganizationService;

import java.util.List;

@Controller
@RequestMapping("organization")
public class OrganizationController {
    @Autowired
    private OrganizationService organizationService;
    @GetMapping("getAll")
    public String getAllOrganization(Model model){
        List<Organization> organizations = organizationService.getAll();
        model.addAttribute("organizations",organizations);
        return "organization/listorganization";
    }

    @GetMapping("create")
    public String showCreateForm(Model model) {
        Organization organization = new Organization();
        model.addAttribute("organization", organization);
        return "organization/createorganization";
    }

    @PostMapping(value = "save")
    public String addStudent(@ModelAttribute("organization") Organization organization, BindingResult result) {
        if (result.hasErrors()) {
            return "organization/createorganization";
        }
        organizationService.create(organization);
        return "redirect:getAll";

    }

    @GetMapping("edit/{id}")
    public ModelAndView showCreateForm(@PathVariable(name = "id") Long id) {
        ModelAndView mav = new ModelAndView("organization/editorganization");
        Organization organization = organizationService.getById(id);
        mav.addObject("organization", organization);
        return mav;
    }

    @PostMapping(value = "update")
    public String update(@ModelAttribute("organization") Organization organization, BindingResult result) {
        if (result.hasErrors()) {
            return "organization/editorganization";
        }
        organizationService.update(organization);
        return "redirect:/organization/getAll";

    }

    @GetMapping("delete/{id}")
    public String deleteDocType(@PathVariable(name = "id") Long id) {
        organizationService.delete(id);
        return "redirect:/organization/getAll";
    }

}
