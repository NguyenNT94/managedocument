package rc.bootsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import rc.bootsecurity.model.User;
import rc.bootsecurity.repository.UserRepository;
import rc.bootsecurity.service.UserService;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @GetMapping("/getAll")
    public String getAllUser(Model model){
        List<User> users = userService.getAllUser();
        model.addAttribute("users",users);
        return "teacher/listteacher";
    }

    @GetMapping("create")
    public String showCreateForm(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "teacher/createteacher";
    }

    @PostMapping(value = "save")
    public String addStudent(@ModelAttribute("user") User user, BindingResult result) {
        if (result.hasErrors()) {
            return "teacher/createteacher";
        }
        userService.create(user);
        return "redirect:/user/getAll";

    }
    @GetMapping("/profile")
    public ModelAndView getUserProfile(Principal principal){
        String username = principal.getName();
        ModelAndView mav = new ModelAndView("profile/edituserprofile");
        User user = userRepository.findByUsername(username);
        mav.addObject("user", user);
        return mav;
    }
    @PostMapping(value = "update")
    public String update(@ModelAttribute("user") User user, BindingResult result) {
        if (result.hasErrors()) {
            return "profile/edituserprofile";
        }
        userService.update(user);
        return "redirect:/logout";

    }

    @GetMapping("delete/{username}")
    public String deleteDocType(@PathVariable(name = "username") String username) {
        userService.delete(username);
        return "redirect:/user/getAll";
    }
}
