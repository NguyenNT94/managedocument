package rc.bootsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rc.bootsecurity.model.*;
import rc.bootsecurity.repository.LevelDocRepository;
import rc.bootsecurity.repository.UserRepository;
import rc.bootsecurity.service.DocumentPassService;
import rc.bootsecurity.service.DocumentTypeService;
import rc.bootsecurity.service.OrganizationService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("documentpass")
public class DocumentPassController {
    @Autowired
    private DocumentPassService documentPassService;
    @Autowired
    private LevelDocRepository levelDocRepository;
    @Autowired
    private DocumentTypeService documentTypeService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private OrganizationService organizationService;
    private static final String UPLOAD_FOLDER = "C:\\temp\\";
    @GetMapping("getAll")
    public String getAllDocument(Model model, Principal principal){

        User usertmp = userRepository.findByUsername(principal.getName());
        List<String> ls = new ArrayList<>();
        String[] levels = usertmp.getPermissions().split(",");
        ls.addAll(Arrays.asList(levels));
        List<DocumentPass> documentPasses = documentPassService.findAllDoc(ls);
        model.addAttribute("documentPass",documentPasses);
        return "documentpass/listdocument";
    }

    @GetMapping("create")
    public String showCreateForm(Model model) {
        DocumentPass documentpass = new DocumentPass();
        List<LevelDoc> levelDocs = levelDocRepository.findAll();
        List<DocType> docTypes = documentTypeService.getAll();
        List<Organization> organizations = organizationService.getAll();
        model.addAttribute("leveldocs",levelDocs);
        model.addAttribute("documentpass", documentpass);
        model.addAttribute("doctypes", docTypes);
        model.addAttribute("organizations",organizations);
        return "documentpass/createdocumentpass";
    }

    @PostMapping("save")
    public String saveDocument(@ModelAttribute("documentpass")DocumentPass documentpass, @RequestParam("filename") MultipartFile myFile, @RequestParam(value = "dateSign") String dateSign, @RequestParam(value = "dateSent") String dateSent, BindingResult result){
        if (result.hasErrors()) {
            return "documentpass/createdocumentpass";
        }
        try {
            Date daysign=new SimpleDateFormat("yyyy-MM-dd").parse(dateSign);
            Date daysent=new SimpleDateFormat("yyyy-MM-dd").parse(dateSent);
            String typeFile = myFile.getOriginalFilename().substring(myFile.getOriginalFilename().length() - 3);
            byte[] image = myFile.getBytes();
            documentpass.setContent(image);
            documentpass.setDateSend(daysent);
            documentpass.setDaySign(daysign);
            documentpass.setTypeFile(typeFile);
            documentPassService.create(documentpass);
            return "redirect:/documentpass/getAll";
        }
        catch (Exception ex){
            return "redirect:/documentpass/create";
        }

    }
    @GetMapping("download/{id}")
    public String showCreateForm(@PathVariable(name = "id") String id) throws IOException {

        DocumentPass documentPass = documentPassService.getById(id);

        try {
            writeBytesToFile(documentPass.getContent(), UPLOAD_FOLDER + documentPass.getTitle()+"."+ documentPass.getTypeFile());
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return "redirect:/documentpass/getAll";
    }

    @GetMapping("view/{id}")
    public String showFilePdf(@PathVariable(name = "id") String id, HttpServletResponse response) throws IOException {

        DocumentPass documentPass = documentPassService.getById(id);


        try {
            writeBytesToFile(documentPass.getContent(), UPLOAD_FOLDER + documentPass.getTitle()+"."+ documentPass.getTypeFile());
            String path = UPLOAD_FOLDER + documentPass.getTitle()+"."+ documentPass.getTypeFile();
            path =  path.replace("\\", "\\\\");
            File file = new File(path);

            FileInputStream inputStream = new FileInputStream(file);
            byte[] buffer = new byte[8192];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1)
            {
                baos.write(buffer, 0, bytesRead);
            }
            response.setHeader("Content-Disposition","inline; filename=\""+file.getName()+"\"");
            if(documentPass.getTypeFile()=="png"){
                response.setContentType("image/png");
            }
            else {
                response.setContentType("application/pdf");
            }

            ServletOutputStream outputStream = response.getOutputStream();
            baos.writeTo(outputStream);
            outputStream.flush();

        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return "redirect:/documentpass/getAll";
    }
    @GetMapping("delete/{id}")
    public String deleteDocType(@PathVariable(name = "id") String id) {
        documentPassService.delete(id);
        return "redirect:/documentpass/getAll";
    }

    private static void writeBytesToFile(byte[] bFile, String fileDest) {

        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
            fileOuputStream.write(bFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
