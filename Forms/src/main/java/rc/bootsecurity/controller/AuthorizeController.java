package rc.bootsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rc.bootsecurity.model.Authorization;
import rc.bootsecurity.model.LevelDoc;
import rc.bootsecurity.model.User;
import rc.bootsecurity.repository.LevelDocRepository;
import rc.bootsecurity.repository.UserRepository;
import rc.bootsecurity.service.UserService;

import java.util.List;

@Controller
@RequestMapping("authorize")
public class AuthorizeController {
    @Autowired
    private UserService userService;
    @Autowired
    private LevelDocRepository levelDocRepository;
    @Autowired
    private UserRepository userRepository;
    @GetMapping("getAll")
    public String showAllUser(Model model){
        List<User> users = userService.getAllUser();
        List<LevelDoc> levelDocs = levelDocRepository.findAll();
        Authorization authorization = new Authorization();
        for (User user : users) {
            if(user.getUsername()=="admin"){
                users.remove(user);
                break;
            }
        }
        model.addAttribute("users", users);
        model.addAttribute("authorization",authorization);
        model.addAttribute("levelDocs",levelDocs);
        return "authorization/authorizeform";
    }
//    @PostMapping("update")
//    public String updateAuthorize(@RequestParam(value = "levels", required = false) List<String> levels){
//
//        return "authorization/authorizeform";
//    }
    @PostMapping("update")
    public String updateAuthorize(@ModelAttribute("authorization")Authorization authorization, @RequestParam(value = "levels", required = false) List<String> levels, BindingResult result){
        if (result.hasErrors()) {
            return "authorization/getAll";
        }
        try {
            User usertmp = userRepository.findByUsername(authorization.getUsername());
            usertmp.setActive(1);
            usertmp.setRoles(authorization.getRole());
            String permission = "";
            if(!levels.isEmpty() || levels!=null){
                for (String level: levels){
                    permission = permission + level + ",";
                }
                permission = permission.substring(0, permission.length()-1);
                usertmp.setPermissions(permission);
                userRepository.save(usertmp);
            }
        }
        catch(Exception ex){
            return "redirect:/authorize/getAll";
        }

        return "redirect:/user/getAll";
    }

}
