package rc.bootsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import rc.bootsecurity.model.DocType;
import rc.bootsecurity.service.DocumentTypeService;

import java.util.List;

@Controller
@RequestMapping("document-type")
public class DocumentTypeController {
    @Autowired
    private DocumentTypeService documentTypeService;
    @GetMapping("getAll")
    public String getAllDocumentType(Model model){
        List<DocType> documentTypes = documentTypeService.getAll();
        model.addAttribute("documenttypes",documentTypes);
        return "documenttype/documenttype";
    }
    @GetMapping("create")
    public String showCreateForm(Model model) {
        DocType docType = new DocType();
        model.addAttribute("docType", docType);
        return "documenttype/createdoctype";
    }

    @PostMapping(value = "save")
    public String addStudent(@ModelAttribute("docType") DocType docType, BindingResult result) {
        if (result.hasErrors()) {
            return "documenttype/createdoctype";
        }
        documentTypeService.create(docType);
        return "redirect:getAll";

    }
    @GetMapping("edit/{id}")
    public ModelAndView showCreateForm(@PathVariable(name = "id") String id) {
       ModelAndView mav = new ModelAndView("documenttype/editdoctype");
       DocType docType = documentTypeService.getById(id);
       mav.addObject("docType", docType);
        return mav;
    }

    @PostMapping(value = "update")
    public String update(@ModelAttribute("docType") DocType docType, BindingResult result) {
        if (result.hasErrors()) {
            return "documenttype/editdoctype";
        }
        documentTypeService.update(docType);
        return "redirect:getAll";

    }

    @GetMapping("delete/{id}")
    public String deleteDocType(@PathVariable(name = "id") String id) {
        documentTypeService.delete(id);
        return "redirect:/document-type/getAll";
    }

}
