package rc.bootsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import rc.bootsecurity.model.*;
import rc.bootsecurity.repository.LevelDocRepository;
import rc.bootsecurity.repository.UserRepository;
import rc.bootsecurity.service.DocumentArrService;
import rc.bootsecurity.service.DocumentTypeService;
import rc.bootsecurity.service.OrganizationService;
import rc.bootsecurity.service.UserService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Principal;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("documentarr")
public class DocumentArrController {
    @Autowired
    private DocumentArrService documentArrService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LevelDocRepository levelDocRepository;
    @Autowired
    private DocumentTypeService documentTypeService;
    @Autowired
    private OrganizationService organizationService;
    private static final String UPLOAD_FOLDER = "C:\\temp\\";
    @GetMapping("getAll")
    public String getAllDocument(Model model, Principal principal){
        User usertmp = userRepository.findByUsername(principal.getName());
        List<String> ls = new ArrayList<>();
        String[] levels = usertmp.getPermissions().split(",");
        ls.addAll(Arrays.asList(levels));
        List<DocumentArr> documentArrs = documentArrService.findAllDoc(ls);
        model.addAttribute("documentArrs",documentArrs);
        return "documentarr/listdocument";

    }
    @GetMapping("create")
        public String showCreateForm(Model model) {
            DocumentArr documentarr = new DocumentArr();
            List<LevelDoc> levelDocs = levelDocRepository.findAll();
            List<DocType> docTypes = documentTypeService.getAll();
            List<Organization> organizations = organizationService.getAll();
            model.addAttribute("leveldocs",levelDocs);
            model.addAttribute("documentarr", documentarr);
            model.addAttribute("doctypes", docTypes);
            model.addAttribute("organizations",organizations);
            return "documentarr/createdocumentarr";
    }

    @PostMapping("save")
    public String updateAuthorize(@ModelAttribute("documentarr")DocumentArr documentarr, @RequestParam("filename") MultipartFile myFile, @RequestParam(value = "dateSign") String dateSign, @RequestParam(value = "dateSent") String dateSent, BindingResult result){
        if (result.hasErrors()) {
            return "documentarr/createdocumentarr";
        }
        try {
            Date daysign=new SimpleDateFormat("yyyy-MM-dd").parse(dateSign);
            Date daysent=new SimpleDateFormat("yyyy-MM-dd").parse(dateSent);
            String typeFile = myFile.getOriginalFilename().substring(myFile.getOriginalFilename().length() - 3);
            byte[] image = myFile.getBytes();
            documentarr.setContent(image);
            documentarr.setDateSend(daysent);
            documentarr.setDaySign(daysign);
            documentarr.setTypefile(typeFile);
            documentArrService.create(documentarr);
            return "redirect:/documentarr/getAll";
        }
        catch (Exception ex){
            return "redirect:/documentarr/create";
        }

    }
    @GetMapping("download/{id}")
    public String showCreateForm(@PathVariable(name = "id") String id) throws IOException {

        DocumentArr documentArr = documentArrService.getById(id);
        try {
            writeBytesToFile(documentArr.getContent(), UPLOAD_FOLDER + documentArr.getTitle()+"."+ documentArr.getTypefile());
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return "redirect:/documentarr/getAll";
    }

    @GetMapping("view/{id}")
    public String showFilePdf(@PathVariable(name = "id") String id, HttpServletResponse response) throws IOException {

        DocumentArr documentArr = documentArrService.getById(id);


        try {
            writeBytesToFile(documentArr.getContent(), UPLOAD_FOLDER + documentArr.getTitle()+"."+ documentArr.getTypefile());
            String path = UPLOAD_FOLDER + documentArr.getTitle()+"."+ documentArr.getTypefile();
            path =  path.replace("\\", "\\\\");
            File file = new File(path);

            FileInputStream inputStream = new FileInputStream(file);
            byte[] buffer = new byte[8192];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1)
            {
                baos.write(buffer, 0, bytesRead);
            }
            response.setHeader("Content-Disposition","inline; filename=\""+file.getName()+"\"");
           if(documentArr.getTypefile()=="png"){
               response.setContentType("image/png");
           }
            response.setContentType("application/pdf");

            ServletOutputStream outputStream = response.getOutputStream();
            baos.writeTo(outputStream);
            outputStream.flush();

        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return "redirect:/documentarr/getAll";
    }
    @GetMapping("delete/{id}")
    public String deleteDocType(@PathVariable(name = "id") String id) {
        documentArrService.delete(id);
        return "redirect:/documentarr/getAll";
    }

    private static void writeBytesToFile(byte[] bFile, String fileDest) {

        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
            fileOuputStream.write(bFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
