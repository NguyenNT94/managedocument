package rc.bootsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import rc.bootsecurity.model.User;
import rc.bootsecurity.repository.UserRepository;
import rc.bootsecurity.service.UserService;

import java.util.List;

@Controller
public class HomeController {
    @Autowired
    private UserService userService;
    @GetMapping("")
    public String login(){
        return "login";
    }
    @GetMapping("403")
    public String error(){
        return "403";
    }
    @GetMapping("/index")
    public String index(){return "403";}
    @GetMapping("/login")
    public String login1(){return "login";}
    @GetMapping("/dashboard")
    public String dashboard(Model model){

        return "dashboard";
    }
    @GetMapping("/users")
    public String asd(){return "admin/users";}
}
