package rc.bootsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import rc.bootsecurity.model.DocumentArr;
import rc.bootsecurity.service.DocumentArrService;
import rc.bootsecurity.service.TestService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("admin")
public class AdminController {

    @Autowired
    private TestService documentArrService;

    @GetMapping("index")
    public String index(){
        return "admin/index";
    }

    @GetMapping("list")
    public String getAllUser(){
        return "users";
    }

    @GetMapping("getAll")
    public String getAllDocument(Model model){
        List<String> ls = new ArrayList<>();
        ls.add("MAT");
        ls.add("THUONG");
        List<DocumentArr> documentArrs = documentArrService.findAllDoc(ls);
        model.addAttribute("documentArrs",documentArrs);
        return "documentarr/listdocument";
    }
}
