package rc.bootsecurity.db;

import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rc.bootsecurity.model.LevelDoc;
import rc.bootsecurity.model.User;
import rc.bootsecurity.repository.LevelDocRepository;
import rc.bootsecurity.repository.UserRepository;

import java.util.Arrays;
import java.util.List;

@Service
public class DbInit implements CommandLineRunner {
    private UserRepository userRepository;
    private LevelDocRepository levelDocRepository;
    private PasswordEncoder passwordEncoder;

    public DbInit(UserRepository userRepository, PasswordEncoder passwordEncoder, LevelDocRepository levelDocRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.levelDocRepository = levelDocRepository;
    }

    @Override
    public void run(String... args) {
        this.userRepository.deleteAll();
        this.levelDocRepository.deleteAll();
        User dan = new User("user",passwordEncoder.encode("user"),"USER","", "Lambogini", "nguyennt@gmail.com");
        User admin = new User("admin",passwordEncoder.encode("admin"),"ADMIN","TUYET_MAT,TOI_MAT,KHAN,THUONG_KHAN,HOA_TOC,THUONG", "Nguyễn Thanh Nguyên", "nguyennt47b2@gmail.com");


        List<User> users = Arrays.asList(dan,admin);
        this.userRepository.saveAll(users);
        LevelDoc lv1 = new LevelDoc("TUYET_MAT", "Tuyệt Mật");
        LevelDoc lv2 = new LevelDoc("TOI_MAT", "Tối mật");
        LevelDoc lv3 = new LevelDoc("MAT", "Mật");
        LevelDoc lv4 = new LevelDoc("KHAN", "Khẩn");
        LevelDoc lv5 = new LevelDoc("THUONG_KHAN", "Thượng Khẩn");
        LevelDoc lv6 = new LevelDoc("HOA_TOC", "Hỏa Tốc Hẹn Giờ");
        LevelDoc lv7 = new LevelDoc("THUONG", "Thường");
        List<LevelDoc> levelDocs = Arrays.asList(lv1,lv2,lv3,lv4,lv5,lv6,lv7);
        this.levelDocRepository.saveAll(levelDocs);
    }
}
