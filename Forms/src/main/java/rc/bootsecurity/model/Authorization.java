package rc.bootsecurity.model;

import java.util.List;

public class Authorization {
    private String username;
    private String role;
    private String[] levels;
    public Authorization(){}
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String[] getLevels() {
        return levels;
    }

    public void setLevels(String[] levels) {
        this.levels = levels;
    }
}
