package rc.bootsecurity.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DocType {
    @Id
    private String id;
    private String name;
    private String description;
    public DocType(){

    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        description = description;
    }
}
