package rc.bootsecurity.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.util.Date;

@Entity
public class DocumentPass {
    @Id
    private  String numberDoc;
    private  String title;
    private String typeDoc;
    @Lob
    @Column(length = 100000)
    private byte[] content;
    private Date dateSend;
    private String organization;
    private String level;
    private Date daySign;
    private String signPer;
    private String typeFile;

    public String getTypeFile() {
        return typeFile;
    }

    public void setTypeFile(String typeFile) {
        this.typeFile = typeFile;
    }

    public DocumentPass(){};

    public DocumentPass(String numberDoc, String title, String typeDoc, byte[] content, Date dateSend, String organization, String level, Date daySign, String signPer) {
        this.numberDoc = numberDoc;
        this.title = title;
        this.typeDoc = typeDoc;
        this.content = content;
        this.dateSend = dateSend;
        this.organization = organization;
        this.level = level;
        this.daySign = daySign;
        this.signPer = signPer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getNumberDoc() {
        return numberDoc;
    }

    public void setNumberDoc(String numberDoc) {
        this.numberDoc = numberDoc;
    }

    public String getTypeDoc() {
        return typeDoc;
    }

    public void setTypeDoc(String typeDoc) {
        this.typeDoc = typeDoc;
    }

    public Date getDateSend() {
        return dateSend;
    }

    public void setDateSend(Date dateSend) {
        this.dateSend = dateSend;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public void setDaySign(Date daySign) {
        this.daySign = daySign;
    }

    public Date getDaySign() {
        return daySign;
    }


    public String getSignPer() {
        return signPer;
    }

    public void setSignPer(String signPer) {
        this.signPer = signPer;
    }
}
