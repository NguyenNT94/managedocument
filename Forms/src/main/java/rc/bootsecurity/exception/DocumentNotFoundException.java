package rc.bootsecurity.exception;

public class DocumentNotFoundException extends RuntimeException {
    public DocumentNotFoundException(String message) {
        super(String.format("Not found document %s",message));
    }
}
