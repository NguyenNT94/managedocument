package rc.bootsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rc.bootsecurity.model.LevelDoc;

@Repository
public interface LevelDocRepository extends JpaRepository<LevelDoc,String> {
}
