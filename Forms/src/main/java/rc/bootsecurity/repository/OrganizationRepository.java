package rc.bootsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rc.bootsecurity.model.Organization;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long> {
}
