package rc.bootsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rc.bootsecurity.model.DocType;

@Repository
public interface DocumentTypeRepository extends JpaRepository<DocType, String> {

}
