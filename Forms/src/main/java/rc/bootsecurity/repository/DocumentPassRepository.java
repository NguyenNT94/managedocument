package rc.bootsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import rc.bootsecurity.model.DocumentPass;

import java.util.List;

@Repository
public interface DocumentPassRepository extends JpaRepository<DocumentPass, String> {
    @Query("SELECT u FROM DocumentPass u WHERE u.level = ?1")
    List<DocumentPass> findAllDoc(String levels);
}
