package rc.bootsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import rc.bootsecurity.model.DocumentArr;

import java.util.List;

@Repository
public interface DocumentArrRepository extends JpaRepository<DocumentArr,String> {
    @Query("SELECT u FROM DocumentArr u WHERE u.level = ?1")
    List<DocumentArr> findAllDoc(String levels);
}
