package rc.bootsecurity.service;

import rc.bootsecurity.model.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    List<User> getAllUser();
    User update(User user);
    User create(User user);
    String delete(String username);
}
