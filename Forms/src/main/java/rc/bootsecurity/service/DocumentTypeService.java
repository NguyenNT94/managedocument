package rc.bootsecurity.service;

import rc.bootsecurity.model.DocType;

import java.util.List;

public interface DocumentTypeService {
    List<DocType> getAll();
    DocType getById(String id);
    DocType create(DocType documentType);
    String delete(String id);
    DocType update(DocType documentType);

}
