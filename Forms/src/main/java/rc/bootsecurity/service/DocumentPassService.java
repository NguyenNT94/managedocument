package rc.bootsecurity.service;

import rc.bootsecurity.model.DocumentPass;

import java.util.List;

public interface DocumentPassService {
    List<DocumentPass> getAll();
    DocumentPass getById(String id);
    DocumentPass create(DocumentPass documentArr);
    String delete(String id);
    DocumentPass update(DocumentPass documentArr);
    List<DocumentPass> findAllDoc(List<String> levels);
}
