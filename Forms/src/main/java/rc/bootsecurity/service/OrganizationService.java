package rc.bootsecurity.service;

import rc.bootsecurity.model.Organization;

import java.util.List;

public interface OrganizationService {
    List<Organization> getAll();
    Organization getById(Long id);
    Organization create(Organization organization);
    String delete(Long id);
    Organization update(Organization organization);
}
