package rc.bootsecurity.service;

import rc.bootsecurity.model.DocumentArr;

import java.util.List;

public interface TestService {
    List<DocumentArr> findAllDoc(List<String> levels);
}
