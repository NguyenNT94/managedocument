package rc.bootsecurity.service;

import rc.bootsecurity.model.DocumentArr;

import java.util.List;

public interface DocumentArrService {
    List<DocumentArr> getAll();
    DocumentArr getById(String id);
    DocumentArr create(DocumentArr documentArr);
    String delete(String id);
    DocumentArr update(DocumentArr documentArr);
    List<DocumentArr> findAllDoc(List<String> levels);
}
