package rc.bootsecurity.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import rc.bootsecurity.model.DocumentArr;
import rc.bootsecurity.repository.TestRepository;
import rc.bootsecurity.service.TestService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class TestImpl implements TestService {
    @Autowired
    private TestRepository testRepository;
    @Override
    public List<DocumentArr> findAllDoc(List<String> levels) {
        List<DocumentArr> listOfficle = new ArrayList<>();
        List<DocumentArr> tmp;
        if(levels.size() == 1){
            listOfficle = testRepository.findAllDoc(levels.get(0));
        }
        else {
            for (String level:levels) {
                tmp = testRepository.findAllDoc(level);
                listOfficle.addAll(tmp);
            }
        }
        return listOfficle;
    }


}
