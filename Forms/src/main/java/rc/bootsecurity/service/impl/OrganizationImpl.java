package rc.bootsecurity.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rc.bootsecurity.exception.DocumentNotFoundException;
import rc.bootsecurity.model.Organization;
import rc.bootsecurity.repository.OrganizationRepository;
import rc.bootsecurity.service.OrganizationService;

import java.util.List;
import java.util.Optional;

@Service
public class OrganizationImpl implements OrganizationService {
    @Autowired
    private OrganizationRepository organizationRepository;
    @Override
    public List<Organization> getAll() {
        List<Organization> organizations = organizationRepository.findAll();

        return organizations;
    }

    @Override
    public Organization getById(Long id) {
        Optional<Organization> optional = organizationRepository.findById(id);
        if(optional.isPresent()){
            return optional.get();
        }
        throw new DocumentNotFoundException(id.toString());
    }

    @Override
    public Organization create(Organization organization) {
        Optional<Organization> optional = organizationRepository.findById(organization.getId());

        if (optional.isPresent()) {
            throw new DocumentNotFoundException(String.format("The %s code is already existence", organization.getId()));
        }
        return organizationRepository.save(organization);
    }

    @Override
    public Organization update(Organization organization) {
        return organizationRepository.save(organization);
    }

    @Override
    public String delete(Long id) {
        Optional<Organization> optional = organizationRepository.findById(id);
        if (!optional.isPresent()) {
            throw new DocumentNotFoundException(id.toString());
        }
        Organization documentType = optional.get();
        organizationRepository.delete(documentType);
        return String.format("Delete product with code is %s successful", id);
    }
}
