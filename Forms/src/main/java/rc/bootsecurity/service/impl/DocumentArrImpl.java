package rc.bootsecurity.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rc.bootsecurity.exception.DocumentNotFoundException;
import rc.bootsecurity.model.DocumentArr;
import rc.bootsecurity.model.DocumentPass;
import rc.bootsecurity.repository.DocumentArrRepository;
import rc.bootsecurity.repository.DocumentPassRepository;
import rc.bootsecurity.service.DocumentArrService;
import rc.bootsecurity.service.DocumentPassService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DocumentArrImpl implements DocumentArrService{

    @Autowired
    private DocumentArrRepository documentArrRepository;

    @Override
    public List<DocumentArr> getAll() {
        return documentArrRepository.findAll();
    }

    @Override
    public DocumentArr getById(String id) {
        Optional<DocumentArr> optional = documentArrRepository.findById(id);
        if(optional.isPresent()){
            return optional.get();
        }
        throw new DocumentNotFoundException(id);
    }

    @Override
    public DocumentArr create(DocumentArr documentArr) {
        Optional<DocumentArr> optional = documentArrRepository.findById(documentArr.getNumberDoc());

        if (optional.isPresent()) {
            throw new DocumentNotFoundException(String.format("The %s code is already existence", documentArr.getNumberDoc()));
        }
        return documentArrRepository.save(documentArr);
    }

    @Override
    public String delete(String id) {
        Optional<DocumentArr> optional = documentArrRepository.findById(id);
        if (!optional.isPresent()) {
            throw new DocumentNotFoundException(id);
        }
        DocumentArr documentType = optional.get();
        documentArrRepository.delete(documentType);
        return String.format("Delete product with code is %s successful", id);
    }

    @Override
    public DocumentArr update(DocumentArr documentArr) {
        return documentArrRepository.save(documentArr);
    }

    @Override
    public List<DocumentArr> findAllDoc(List<String> levels) {
        List<DocumentArr> listOfficle = new ArrayList<>();
        List<DocumentArr> tmp;
        if(levels.size() == 1){
            listOfficle = documentArrRepository.findAllDoc(levels.get(0));
        }
        else {
            for (int i = 0;i<levels.size();i++){
                tmp = documentArrRepository.findAllDoc(levels.get(i));
                listOfficle.addAll(tmp);
            }
        }
        return listOfficle;
    }
}
