package rc.bootsecurity.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rc.bootsecurity.exception.DocumentNotFoundException;
import rc.bootsecurity.model.DocType;
import rc.bootsecurity.repository.DocumentTypeRepository;
import rc.bootsecurity.service.DocumentTypeService;

import java.util.List;
import java.util.Optional;

@Service
public class DocumentTypeImpl implements DocumentTypeService {
    @Autowired
    private DocumentTypeRepository documentTypeRepository;

    @Override
    public DocType update(DocType documentType) {
        return documentTypeRepository.save(documentType);
    }

    @Override
    public List<DocType> getAll() {
        List<DocType> documentTypes = documentTypeRepository.findAll();
        return documentTypes;
    }

    @Override
    public DocType getById(String id) {
        Optional<DocType> optional = documentTypeRepository.findById(id);
        if(optional.isPresent()){
            return optional.get();
        }
        throw new DocumentNotFoundException(id);
    }

    @Override
    public DocType create(DocType documentType) {
        Optional<DocType> optional = documentTypeRepository.findById(documentType.getId());

        if (optional.isPresent()) {
            throw new DocumentNotFoundException(String.format("The %s code is already existence", documentType.getId()));
        }
        return documentTypeRepository.save(documentType);
    }

    @Override
    public String delete(String id) {
        Optional<DocType> optional = documentTypeRepository.findById(id);
        if (!optional.isPresent()) {
            throw new DocumentNotFoundException(id);
        }
        DocType documentType = optional.get();
        documentTypeRepository.delete(documentType);
        return String.format("Delete product with code is %s successful", id);
    }
}
