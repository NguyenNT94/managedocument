package rc.bootsecurity.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rc.bootsecurity.exception.DocumentNotFoundException;
import rc.bootsecurity.model.User;
import rc.bootsecurity.repository.UserRepository;
import rc.bootsecurity.service.UserService;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    public UserServiceImpl(PasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User create(User user) {
        User usertmp= userRepository.findByUsername(user.getUsername());

        if (usertmp!=null) {
            throw new DocumentNotFoundException(String.format("The %s code is already existence", usertmp.getUsername()));
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public String delete(String username) {
        User user = userRepository.findByUsername(username);
        userRepository.delete(user);
        return String.format("Delete product with code is %s successful", username);
    }

    @Override
    public User update(User user) {
        User usertmp = userRepository.findByUsername(user.getUsername());
        usertmp.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(usertmp);
    }

    @Override
    public List<User> getAllUser() {
        List<User> users = userRepository.findAll();
        return users;
    }

}
