package rc.bootsecurity.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rc.bootsecurity.exception.DocumentNotFoundException;
import rc.bootsecurity.model.DocumentArr;
import rc.bootsecurity.model.DocumentPass;
import rc.bootsecurity.repository.DocumentArrRepository;
import rc.bootsecurity.repository.DocumentPassRepository;
import rc.bootsecurity.service.DocumentArrService;
import rc.bootsecurity.service.DocumentPassService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DocumentPassImpl implements DocumentPassService {
    @Autowired
    private DocumentPassRepository documentPassRepository;

    @Override
    public List<DocumentPass> getAll() {
        return documentPassRepository.findAll();
    }

    @Override
    public DocumentPass getById(String id) {
        Optional<DocumentPass> optional = documentPassRepository.findById(id);
        if(optional.isPresent()){
            return optional.get();
        }
        throw new DocumentNotFoundException(id);
    }

    @Override
    public DocumentPass create(DocumentPass documentPass) {
        Optional<DocumentPass> optional = documentPassRepository.findById(documentPass.getNumberDoc());

        if (optional.isPresent()) {
            throw new DocumentNotFoundException(String.format("The %s code is already existence", documentPass.getNumberDoc()));
        }
        return documentPassRepository.save(documentPass);
    }

    @Override
    public String delete(String id) {
        Optional<DocumentPass> optional = documentPassRepository.findById(id);
        if (!optional.isPresent()) {
            throw new DocumentNotFoundException(id);
        }
        DocumentPass documentPass = optional.get();
        documentPassRepository.delete(documentPass);
        return String.format("Delete product with code is %s successful", id);
    }

    @Override
    public DocumentPass update(DocumentPass documentPass) {
        return documentPassRepository.save(documentPass);
    }

    @Override
    public List<DocumentPass> findAllDoc(List<String> levels) {
        List<DocumentPass> listOfficle = new ArrayList<>();
        List<DocumentPass> tmp;
        if(levels.size() == 1){
            listOfficle = documentPassRepository.findAllDoc(levels.get(0));
        }
        else {
            for (int i = 0;i<levels.size();i++){
                tmp = documentPassRepository.findAllDoc(levels.get(i));
                listOfficle.addAll(tmp);
            }
        }
        return listOfficle;
    }

}
